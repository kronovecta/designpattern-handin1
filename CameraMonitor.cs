﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CameraApp
{
    /// <summary>
    /// Camera Monitor is holding to many references to other classes, its strongly coupled.
    /// Look at some designpatterns that solve one-to-many relationships. 
    /// </summary>
    
    interface IMonitor
    {
        void Start();
        void Stop();
        void AttachCamera(ICamera camera);
        void DetachCamera(ICamera camera);
    }

    class Monitor : IMonitor
    {
        private readonly List<ICamera> _observers = new List<ICamera>();
        
        public void Start()
        {
            foreach (var camera in _observers)
            {
                Console.WriteLine("\n----------- Camera " + camera.Id + " -----------");
                camera.Start();
            }
        }

        public void Stop()
        {
            foreach (var camera in _observers)
            {
                Console.WriteLine("\n----------- Camera " + camera.Id + " -----------");
                camera.Stop();
            }
        }

        public void AttachCamera(ICamera camera)
        {
            try
            {
                Console.WriteLine("Attempting to attach camera " + camera.Id);
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("\nCamera Type invalid\n");
            }

            if (camera != null)
            {
                _observers.Add(camera);
                Console.WriteLine("Camera " + camera.Id + " attached");
            }
        }

        public void DetachCamera(ICamera camera)
        {   
            _observers.Remove(camera);
            Console.WriteLine("\nCamera " + camera.Id + " detached");
        }
    }
}
