﻿using CameraApp;

// 
// This program is a badly written one. It has strong coupling between objects, no interfaces and no apparent designpatterns implemented.
// The program simulates a command-central, which can start and stop the surveillance cameras connected to it.
// Every camera is represented by a camera class, and in this program there are two types, indoor cameras and outdoor cameras.
// The cameras itself has different implementations, the outdoor cameras also has an lamp that must be handled. 
// 
//
// Camera settings:
//
// #Indoors 
// - Sound ON
// - Light OFF
// - Motion detect OFF
// - Filter OFF
//
// #Outdoors
// - Sound OFF
// - Light ON
// - Motion detect ON
// - Filter ON
//
// ASSIGNMENT
// -----------------------------
//  The goal is to redo the classes below - EXCEPT the camera-API!
//  You need to find a way to use design-patterns that decouple the strong relationship these classes have. 
//  *The user wants an convenient way (maybe by user input) to pick what cameras to use (camera - TYPE) and attach them to the Monitor system. 
//   This means using some kind of Creational pattern.
//  *The user only need one type of camera, indoor or outdoor
//  *The user want the feature to add and remove cameras from the monitor
//  *The user want to be able to send an signal to start or stop the cameras
//  
//
// A possible solution will be provided 20/1 2019 23:59 
// If you can not submit a solution by this date and time for what ever reason, you need to contact me.  
// 
// Sammy Lundqvist
// 0702634744
// sammy_lundqvist@outlook.com

namespace Assignment
{

    public class Program
    {
        static void Main(string[] args)
        {
            //EXAMPLE

            // An instance of the monitor system

            IMonitor monitor = new Monitor();

            // Instantiate the factory

            CameraFactory cameraFactory = new CameraFactory(); // Instansierar kamerafabriken

            // Genererar kameror ur fabriken där inparameter bestämmer typen av kamera
            var cam1 = cameraFactory.CreateCamera("indoor");
            var cam2 = cameraFactory.CreateCamera("outdoor");
            System.Console.WriteLine(cam1.GetType());

            
            // Add them to the monitor
            monitor.AttachCamera(cam1);
            monitor.AttachCamera(cam2);
            
            CameraLogger.Instance.Log(cam1);

            // Just an optional scenario:
            // If time is night(20-07) - start the outdoor cameras

            //TimeSpan start = TimeSpan.Parse("20:00");
            //TimeSpan end = TimeSpan.Parse("07:00");
            //TimeSpan now = DateTime.Now.TimeOfDay;

            //if ((now > start) && (now < end))
            //   monitor.StartCameras();
            //else
            //   monitor.StopCameras();

            monitor.Start();
            monitor.Stop();
            
            monitor.DetachCamera(cam1);
            monitor.DetachCamera(cam2);
        }

    }
}