﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CameraApp
{
    public interface ILogger
    {
        void Log(ICamera device);
    }

    public sealed class CameraLogger : ILogger
    {
        public string FileName { get; set; }
        private static CameraLogger _instance = null;
        private static readonly object locker = new object();

        private CameraLogger() { }

        public static CameraLogger Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (locker)
                    {
                        if (_instance == null)
                        {
                            _instance = new CameraLogger();
                        }
                    }

                }

                return _instance;
            }
        }

        public void Log(ICamera camera)
        {
            //File.AppendAllText("CameraApp.log", "Camera ID: " + camera.Id + ":, snapshot date: " + DateTime.Today.ToString(System.Globalization.CultureInfo.CurrentCulture));
            Console.WriteLine("Camera ID: " + camera.Id + ":, snapshot date: " + DateTime.Now.ToString(System.Globalization.CultureInfo.CurrentCulture));
        }
    }
}
