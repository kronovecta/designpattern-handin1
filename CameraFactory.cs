﻿using CameraAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CameraApp
{
    #region Factory
    // Factory Method Pattern
    public class CameraFactory
    {
        public static int Id { get; private set; }

        public ICamera CreateCamera(string type)
        {
            switch(type)
            {
                case "indoor":
                    return new IndoorCamera(++Id, new IndoorCameraControls());
                case "outdoor":
                    return new OutdoorCamera(++Id, new OutdoorCameraControls());
                default:
                    return null;
            }
        }
    }
    #endregion

    #region Interfaces
    // Skapar en interface för en generell kamera
    public interface ICamera
    {
        int Id { get; set; }

        void Start();
        void Stop();
    }
    #endregion


    public abstract class Camera : ICamera
    {
        public int Id { get; set; }
        ICameraControls controls { get; set; }
         
        public Camera(int id) { Id = id; }

        public abstract void Start();
        public abstract void Stop();
    }

    #region Implementation-classes
    public class IndoorCamera : Camera
    {
        ICameraControls controls { get; set; }
        
        public IndoorCamera(int id, ICameraControls controls) : base(id)
        {
            this.controls = controls;
        }

        public override void Start()
        {
            controls.Start();
        }

        public override void Stop()
        {
            controls.Stop();
        }
    }

    public class OutdoorCamera : Camera
    {
        ICameraControls controls { get; set; }

        public OutdoorCamera(int id, ICameraControls controls) : base(id)
        {
            this.controls = controls;
        }

        public override void Start()
        {
            controls.Start();
        }

        public override void Stop()
        {
            controls.Stop();
        }
    }
    #endregion

    #region Camera Controls
    public interface ICameraControls
    {
        void Start();
        void Stop();
    }

    // Facade design pattern
    // Implementeras för att flytta ut API-dependency från de konkreta kamerorna för ökad polymorphism
    class IndoorCameraControls : ICameraControls
    {
        private ImageProcessor _ImgP;
        private CameraDriver _CamDriver;
        private SoundProcessor _SoundP;

        public IndoorCameraControls()
        {
            _ImgP = new ImageProcessor();
            _CamDriver = new CameraDriver();
            _SoundP = new SoundProcessor();
        }

        public void Start()
        {
            _CamDriver.ConnectCamera();
            _ImgP.StartImageReceiver();
            _SoundP.StartSoundReceiver();
            _SoundP.SetVolume(0.5f);
        }

        public void Stop()
        {
            _SoundP.SetVolume(0f);
            _SoundP.StopSoundReceiver();
            _ImgP.StopImageReceiver();
            _CamDriver.DisconnectCamera();
        }
    }

    class OutdoorCameraControls : ICameraControls
    {
        private ImageProcessor _ImgP;
        private CameraDriver _CamDriver;
        private CameraLight _CamLight;
        private MotionSensor _MotSensor;

        public OutdoorCameraControls()
        {
            _ImgP = new ImageProcessor();
            _CamDriver = new CameraDriver();
            _CamLight = new CameraLight();
            _MotSensor = new MotionSensor();
        }

        public void Start()
        {
            _CamDriver.ConnectCamera();
            _ImgP.StartImageReceiver();
            _MotSensor.StartMotionSensor();
            _CamLight.StartLight();
        }

        public void Stop()
        {
            _CamLight.StopLight();
            _MotSensor.StopMotionSensor();
            _ImgP.StopImageReceiver();
            _CamDriver.DisconnectCamera();
        }
    }
    #endregion
}
